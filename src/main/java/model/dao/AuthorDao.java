package model.dao;

import model.tableObjects.Author;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthorDao {

    public enum AuthorStatement {
        ADD_AUTHOR,
        EDIT_AUTHOR,
        DELETE_AUTHOR,
        FIND_ALL_AUTHORS,
        FIND_ALL_AUTHORNAMES,
        FIND_AUTHOR_BY_ID,
        FIND_AUTHOR_BY_NAME,
        FILTER_BOOK_AUTHOR_BY_AUTHOR_ID
    }

    private Connection connetion;
    private Map<AuthorStatement, String> statements = new HashMap<>();;

    public AuthorDao(Connection connetion) {
        if (connetion == null) {
            throw new IllegalArgumentException();
        }
        this.connetion = connetion;
        prepareStatements();
    }

    private void prepareStatements() {

        String findAuthorByName = "select * from author where name = ?";
        statements.put(AuthorStatement.FIND_AUTHOR_BY_NAME, findAuthorByName);

        String fetchAllAuthors = "select * from author";
        statements.put(AuthorStatement.FIND_ALL_AUTHORS, fetchAllAuthors);

        String fetchAllAuthorNames = "select name from author";
        statements.put(AuthorStatement.FIND_ALL_AUTHORNAMES, fetchAllAuthorNames);

        String addAuthor = "insert into author values(null, ?)";
        statements.put(AuthorStatement.ADD_AUTHOR, addAuthor);

        String editAuthor = "update author set name=? , author_order = ? where id = ?";
        statements.put(AuthorStatement.EDIT_AUTHOR, editAuthor);

        String deleteAuthor = "deleteBookList from author where id = ?";
        statements.put(AuthorStatement.DELETE_AUTHOR, deleteAuthor);

        String findAuthorById = "select  author.id, author.name, bookauthor.author_order from bookauthor\n" +
                "inner join author on bookauthor.author_id= author.id\n" +
                "where bookauthor.book_id = ?";
        statements.put(AuthorStatement.FIND_AUTHOR_BY_ID, findAuthorById);
    }


    public List<Author> fetchAllAuthors() {
        List<Author> authors = new ArrayList<>();
        try {
            PreparedStatement fetchAllAuthors = connetion.prepareStatement(statements.get(AuthorStatement.FIND_ALL_AUTHORS));
            fetchAllAuthors.execute();
            ResultSet resultSet = fetchAllAuthors.getResultSet();
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                authors.add(new Author(id, name, 0));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }

    public List<String > fetchAllAuthorNames() {
        List<String> authorNames = new ArrayList<>();
        try {
            PreparedStatement fetchAllAuthorNames = connetion.prepareStatement(statements.get(AuthorStatement.FIND_ALL_AUTHORNAMES));
            fetchAllAuthorNames.execute();
            ResultSet resultSet = fetchAllAuthorNames.getResultSet();
            while (resultSet.next()) {
//                Integer id = resultSet.getInt(1);
                String name = resultSet.getString(1);
                authorNames.add(name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authorNames;
    }


    public List<Author> findAuthorsById(Integer bookId) {
        List<Author> authors = new ArrayList<>();
        try {
            PreparedStatement findAuthorsById = connetion.prepareStatement(statements.get(AuthorStatement.FIND_AUTHOR_BY_ID));
            findAuthorsById.setInt(1, bookId);
            findAuthorsById.execute();
            ResultSet resultSet = findAuthorsById.getResultSet();
            while (resultSet.next()) {
                authors.add(new Author(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getInt(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }

    public int addAuthor(Author author) {
        int out = 0;
        try {
            PreparedStatement addAuthor = connetion.prepareStatement(statements.get(AuthorStatement.ADD_AUTHOR), Statement.RETURN_GENERATED_KEYS);
            addAuthor.setString(1, author.getName());
            addAuthor.executeUpdate();
            ResultSet authorResult = addAuthor.getGeneratedKeys();
            while (authorResult.next()) {
                out = authorResult.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public void editAuthor(Author author) {
        try {
            PreparedStatement editAuthor = connetion.prepareStatement(statements.get(AuthorStatement.EDIT_AUTHOR));
            editAuthor.setString(1, author.getName());
            editAuthor.setInt(2, author.getAuthorOrder());
            editAuthor.setInt(3, author.getId());
            editAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int deleteAuthor(int authorId) {
        int out = 0;
        try {
            PreparedStatement deleteAuthor = connetion.prepareStatement(statements.get(AuthorStatement.DELETE_AUTHOR));
            deleteAuthor.setInt(1, authorId);
            deleteAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

}




