package model.dao;

import model.tableObjects.Ebook;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EbookDao {




    public enum BookStatement {
        FETCH_ALL_BOOKS,
        ADD_BOOK,
        EDIT_BOOK,
        DELETE_BOOK,
        ADD_BOOK_AUTHOR,
        EDIT_BOOK_AUTHOR,
        DELETE_BOOK_AUTHOR,
        SEARCH_BOOK_BY_TITLE,
        FILTER_BY_AUTHOR,
        FILTER_BY_TAG1,
        FILTER_BY_TAG,
        FILTER_BY_TAG_AUTHOR,
        FILTER_BY_BOOKID,
        FILTER_BY_TAGLIST,
        FILTER_BY_AUTHORLIST,
        FILTER_BY_AUTHOR_TAG_LIST
    }
    private Connection connection;
    private Map<BookStatement, String> statements = new HashMap<>();;

    public EbookDao(Connection connection) {
        this.connection = connection;
        this.statements = new HashMap<>();
        preStatements();
    }

    private void preStatements(){
        String addBook = "insert into book values(null, ?, ?, ?, ?, ?, ?)";
        statements.put(BookStatement.ADD_BOOK, addBook);

        String editBook = "update book set title =?, genre=?, publisher =?, edition=?, year=?, summary=? where id = ?";
        statements.put(BookStatement.EDIT_BOOK, editBook);

        String deleteBook = "delete from book where id = ?";
        statements.put(BookStatement.DELETE_BOOK, deleteBook);

        String fetchAllBooks = "select * from book";
        statements.put(BookStatement.FETCH_ALL_BOOKS, fetchAllBooks);

        String addBookAuthor = "insert into bookAuthor values(?, ?, ?)";
        statements.put(BookStatement.ADD_BOOK_AUTHOR, addBookAuthor);

        String editBookAuthor = "update bookAuthor set author_id = ? where book_id = ? ";
        statements.put(BookStatement.EDIT_BOOK_AUTHOR, editBookAuthor);

        String deleteBookAuthor = "deleteBookList from bookAuthor where book_id = ? and author_id = ?";
        statements.put(BookStatement.DELETE_BOOK_AUTHOR, deleteBookAuthor);

        String searchBookByTitle = "select * from book where title like ?";
        statements.put(BookStatement.SEARCH_BOOK_BY_TITLE, searchBookByTitle);

        String filterByAuthor = "select distinct b.id,title, genre, publisher, edition,year, summary,group_concat(name) as name from book b join bookauthor c on b.id = c.book_id " +
                                "join author a  on a.id=c.author_id where name = ? group by book_id order by author_id";
        statements.put(BookStatement.FILTER_BY_AUTHOR, filterByAuthor);

        String filterByTag1 = "select distinct group_concat(name) as name from author a join bookauthor c on c.author_id = a.id join" +
                " book b on c.book_id = b.id where title = ?";
        statements.put(BookStatement.FILTER_BY_TAG1, filterByTag1);

        String filterByTag = "select distinct b.id,title, genre, publisher, edition,year, summary from book b join booktag tb on b.id = tb.book_id " +
                "join Tag t  on tb.tag_id=t.id where label = ? group by book_id";
        statements.put(BookStatement.FILTER_BY_TAG, filterByTag);

        String filterByBookId = "select * from book where id=Any(?)";
        statements.put(BookStatement.FILTER_BY_BOOKID, filterByBookId);

        String filterByTagAuthor = "select * from book b join booktag bt on b.id = bt.book_id " +
                "join Tag t on bt.tag_id=t.id where label = ? group by book_id and id in ("+filterByAuthor+")";
        statements.put(BookStatement.FILTER_BY_TAG_AUTHOR, filterByTagAuthor);

        String filterByAuthorList = "select * from book join bookauthor on book.id=bookauthor.book_id join author on bookauthor.author_id = author.id where author.name in(?)";
        statements.put(BookStatement.FILTER_BY_AUTHORLIST, filterByAuthorList);

        String filterByTagList = "select book.id from book join booktag on book.id=booktag.book_id join tag on booktag.tag_id = tag.id where tag.label in(?)";
        statements.put(BookStatement.FILTER_BY_TAGLIST, filterByTagList);

        String filterByAuthorTagList = filterByAuthorList+" and book.id in("+filterByTagList+")";
        statements.put(BookStatement.FILTER_BY_AUTHOR_TAG_LIST, filterByAuthorTagList);

    }

    public int addBook(String title, String genre, String publisher, int edition, int year, String summary){
        int out = 0;
        try {
            PreparedStatement addBook = connection.prepareStatement(statements.get(BookStatement.ADD_BOOK), Statement.RETURN_GENERATED_KEYS);
            addBook.setString(1, title);
            addBook.setString(2, genre);
            addBook.setString(3, publisher);
            addBook.setInt(4, edition);
            addBook.setInt(5, year);
            addBook.setString(6, summary);
            addBook.executeUpdate();
            ResultSet bookResult = addBook.getGeneratedKeys();
            while (bookResult.next()){
                out = bookResult.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public void editBook(int id, String title, String genre, String publisher, int edition, int year, String summary){
        try {
            PreparedStatement editBook = connection.prepareStatement(statements.get(BookStatement.EDIT_BOOK));
            editBook.setString(1, title);
            editBook.setString(2, genre);
            editBook.setString(3, publisher);
            editBook.setInt(4, edition);
            editBook.setInt(5, year);
            editBook.setString(6, summary);
            editBook.setInt(7, id);
            editBook.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int deleteBook(Integer bookId){
        int out = 0;
        try {
            PreparedStatement deleteBook = connection.prepareStatement(statements.get(BookStatement.DELETE_BOOK));
            deleteBook.setInt(1, bookId);
            out = deleteBook.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public List<Ebook> fetchAllBooks(){
        Ebook book;
        List<Ebook> books = new ArrayList<>();
        try {
            PreparedStatement fetchAllBooks = connection.prepareStatement(statements.get(BookStatement.FETCH_ALL_BOOKS));
            fetchAllBooks.execute();
            ResultSet resultSet = fetchAllBooks.getResultSet();
            while (resultSet.next()){
                book = new Ebook(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getInt(6),
                        resultSet.getString(7));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }


    public void addBookAuthor(int bookId, int authorId, int authorOrder){
        try {
            PreparedStatement addBookAuthor = connection.prepareStatement(statements.get(BookStatement.ADD_BOOK_AUTHOR));
            addBookAuthor.setInt(1, bookId);
            addBookAuthor.setInt(2, authorId);
            addBookAuthor.setInt(3, authorOrder);
            addBookAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public int deleteBookAuthor(int bookId, int authorId){
        int out =0;
        try {
            PreparedStatement deleteBookAuthor = connection.prepareStatement(statements.get(BookStatement.DELETE_BOOK_AUTHOR));
            deleteBookAuthor.setInt(1, bookId);
            deleteBookAuthor.setInt(2, authorId);
            out = deleteBookAuthor.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return out;
    }

    public List<Ebook> search(String title){
        List<Ebook> searchedBooks = new ArrayList<>();
        try {
            PreparedStatement searchStatement = connection.prepareStatement(statements.get(BookStatement.SEARCH_BOOK_BY_TITLE));

            searchStatement.setString(1,"%"+title+"%");
            ResultSet resultSet = searchStatement.executeQuery();
            while(resultSet.next()){
                Ebook book = new Ebook(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getInt(6),
                        resultSet.getString(7));
                searchedBooks.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  searchedBooks;
    }

    // '   \'hanieh\''   +   ','   +   '\'samaneh\'   '
    public List<Ebook> filterByAuthorList(List<String> authorList){
        PreparedStatement authorsStr = null;
        try {
            authorsStr = connection.prepareStatement("?\',\'?");
//            URLEncoder.encode(authorsStr, authorsStr);
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        for (String author: authorList) {
            try {
                authorsStr.setString(1, authorList.get(0));
                authorsStr.setString(2, authorList.get(1));
            } catch (SQLException e) {
                e.printStackTrace();
            }
//        }

//        authorsStr = authorsStr.substring(0, authorsStr.length()-1);
//        authorsStr = authorsStr.replaceAll(",",",\'");
        System.out.println("authorStr: "+ authorsStr);

        List<Ebook> books = new ArrayList<>();

        try{
            PreparedStatement filterByAuthorListStatement = connection.prepareStatement(statements.get(BookStatement.FILTER_BY_AUTHORLIST));
            filterByAuthorListStatement.setString(1, authorsStr.toString());
            System.out.println("query: "+filterByAuthorListStatement.toString());
            ResultSet resultSet = filterByAuthorListStatement.executeQuery();
            while (resultSet.next()) {
                Ebook book = new Ebook(resultSet.getInt(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4), resultSet.getInt(5),
                        resultSet.getInt(6),resultSet.getString(7));
                books.add(book);
            }
        }
        catch (Exception e){

        }
        return books;
    }

    public List<Ebook> filterByTagList(List<String> tagList){
        String tagsStr = "";
        for (String tag: tagList) {
            tagsStr = tagsStr.concat(tag+", ");
        }
        tagsStr = tagsStr.substring(0, tagsStr.length()-2);
        System.out.println("tagStr: "+ tagsStr);
        List<Ebook> books = new ArrayList<>();

        try{
            PreparedStatement filterByTagListStatement = connection.prepareStatement(statements.get(BookStatement.FILTER_BY_TAGLIST));
            filterByTagListStatement.setString(1, tagsStr);
            ResultSet resultSet = filterByTagListStatement.executeQuery();
            while (resultSet.next()) {
                Ebook book = new Ebook(resultSet.getInt(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4), resultSet.getInt(5),
                        resultSet.getInt(6),resultSet.getString(7));
                books.add(book);
            }
        }
        catch (Exception e){

        }
        return books;
    }

    public List<Ebook> filterByAuthorTagList(List<String> authorList, List<String> tagList) {

        String tagsStr = "";
        for (String tag: tagList) {
            tagsStr = tagsStr.concat(tag+", ");
        }
        tagsStr = tagsStr.substring(0, tagsStr.length()-2);
        System.out.println("tagStr: "+ tagsStr);

        String authorStr = "";
        for (String author: authorList) {
            authorStr = authorStr.concat(author+", ");
        }
        authorStr = authorStr.substring(0, authorStr.length()-2);
        System.out.println("authorStr: "+ authorStr);

        List<Ebook> books = new ArrayList<>();

        try{
            PreparedStatement filterByAuthorTagListStatement = connection.prepareStatement(statements.get(BookStatement.FILTER_BY_AUTHOR_TAG_LIST));
            filterByAuthorTagListStatement.setString(1, authorStr);
            filterByAuthorTagListStatement.setString(2, tagsStr);
            System.out.println("query: "+filterByAuthorTagListStatement.toString());

            ResultSet resultSet = filterByAuthorTagListStatement.executeQuery();
            while (resultSet.next()) {
                Ebook book = new Ebook(resultSet.getInt(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4), resultSet.getInt(5),
                        resultSet.getInt(6),resultSet.getString(7));
                books.add(book);
            }
        }
        catch (Exception e){

        }
        return books;
    }
}
