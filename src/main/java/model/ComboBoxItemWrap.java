package model;


import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;





public class ComboBoxItemWrap<T> {

    private BooleanProperty check = new SimpleBooleanProperty(false);
    private ObjectProperty<T> item = new SimpleObjectProperty<>();

    public ComboBoxItemWrap() {
    }

    public ComboBoxItemWrap(T item) {
        this.item.set(item);
    }

    public ComboBoxItemWrap(T item, Boolean check) {
        this.item.set(item);
        this.check.set(check);
    }

    public BooleanProperty getCheckProperty() {
        return check;
    }

    public Boolean getCheck() {
        return check.getValue();
    }

    public void setCheck(Boolean value) {
        check.set(value);
    }

    public ObjectProperty<T> getItemProperty() {
        return item;
    }

    public T getItem() {
        return item.getValue();
    }

    public void setItem(T value) {
        item.setValue(value);
    }

    @Override
    public String toString() {
        return item.getValue().toString();
    }
}























//
//import javafx.beans.property.BooleanProperty;
//import javafx.beans.property.ObjectProperty;
//import javafx.beans.property.SimpleBooleanProperty;
//import javafx.beans.property.SimpleObjectProperty;
//
//
//public class ComboBoxItemWrap<String> {
//
//
//    private BooleanProperty check = new SimpleBooleanProperty(false);
//    private ObjectProperty<String> item = new SimpleObjectProperty<>();
//
//    ComboBoxItemWrap() {
//    }
//
//    ComboBoxItemWrap(String item) {
//        this.item.set(item);
//    }
//
//    ComboBoxItemWrap(String item, Boolean check) {
//        this.item.set(item);
//        this.check.set(check);
//    }
//
//    public BooleanProperty getCheckProperty() {
//        return check;
//    }
//
//    public Boolean getCheck() {
//        return check.getValue();
//    }
//
//    public void setCheck(Boolean value) {
//        check.set(value);
//    }
//
//    public ObjectProperty<String> getItemProperty() {
//        return item;
//    }
//
//    public String getItem() {
//        return item.getValue();
//    }
//
//    public void setItem(String value) {
//        item.setValue(value);
//    }
//
////    @Override
////    public String toString() {
////        return item.getValue().toString();
////    }
//}
//
//
//
////class Lasi{
////    public Lasi(){
////
////    }
////
////    @FXML
////    private ComboBox tagComboBoxHome;
////    ComboBox<ComboBoxItemWrap<String>> cb = new ComboBox<>();
////    ObservableList<ComboBoxItemWrap<String>> options = FXCollections.observableArrayList(
////            new ComboBoxItemWrap<>("blue"),
////            new ComboBoxItemWrap<>("green"),
////            new ComboBoxItemWrap<>("red"),
////            new ComboBoxItemWrap<>("yellow")
////    );
////
////        cb.setPromptText("Filter");
////
////        cb.setCellFactory( c -> {
////        ListCell<ComboBoxItemWrap<String>> cell = new ListCell<ComboBoxItemWrap<String>>(){
////            @Override
////            protected void updateItem(ComboBoxItemWrap<String> item, boolean empty) {
////                super.updateItem(item, empty);
////                if (!empty) {
////                    final CheckBox cb = new CheckBox(item.toString());
////                    cb.selectedProperty().bind(item.getCheckProperty());
////                    setGraphic(cb);
////                }
////            }
////        };
////
////
////        cell.addEventFilter(MouseEvent.MOUSE_RELEASED, event -> {
////            cell.getItem().getCheckProperty().set(!cell.getItem().getCheckProperty().get());
//////                StringBuilder sb = new StringBuilder();
//////                cb.getItems().filtered( f-> f!=null).filtered( f-> f.getCheck()).forEach( p -> {
//////                    sb.append("; "+p.getItem());
//////                });
//////                final String string = sb.toString();
//////                cb.setPromptText(string.substring(Integer.min(2, string.length())));
////
////        });
////        return cell;
////    });
////
////        cb.setItems(options);
////
//////        ComboBoxListViewSkin comboBoxListViewSkin = new ComboBoxListViewSkin(cb);
//////        comboBoxListViewSkin.getPopupContent().addEventFilter(KeyEvent.ANY, (event) -> {
//////            if(event.getCode() == KeyCode.SPACE) {
//////                event.consume();
//////            }
//////        });
//////        cb.setSkin(comboBoxListViewSkin);
////
////    VBox root = (VBox) scene.getRoot();
////    Button bt = new Button("test");
////        bt.setOnAction(event -> {
////        cb.getItems().filtered( f -> f.getCheck()).forEach( item -> System.out.println(item.getItem()));
////    });
//
////}
