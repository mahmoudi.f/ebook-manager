package model;

/**
 * 2019/08/11 sunday
 * 15:00 PM to 17:00 PM
 * @author : Ghazal Mohammadi
 * pair : Mohammad Ibrahimkhah
 * task : import book data
 *
 *
 * 2019/08/12 monday
 * 11:00 AM to 13:00 PM
 * Mohammad Ibrahimkhah
 * task : adding other book data like authors, tags and comments
 *        adding findAuthors, find tags, find comments methods
 *        change date from String to Date
 *        tested for functionality
 *
 *
 * 2019/08/13 Tuesday
 * 15:00 PM
 * Mohammad Ibrahimkhah
 * task : some refinements
 *
 *
 * 2019/08/14 wednesday
 * 09:30 AM to 11:00 AM
 * Mohammad Ibrahimkhah
 * task : adding zip and unzip
 */


import com.opencsv.CSVReader;
import javafx.scene.paint.Color;
import model.tableObjects.Author;
import model.tableObjects.Comment;
import model.tableObjects.Ebook;
import model.tableObjects.Tag;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Import {
    public List<Ebook> importBooks(String bookDirectory, String  exportFolderAddress, String exportName) throws IOException, ParseException {
        List<Ebook> bookList = new ArrayList<>();

        List<Path> srcFiles = new ArrayList<>();
        Path pathBook = Paths.get(exportFolderAddress + exportName + "_Book.CSV");
        Path pathAuthor = Paths.get(exportFolderAddress + exportName + "_Author.CSV");
        Path pathComment = Paths.get(exportFolderAddress + exportName + "_Comment.CSV");
        Path pathTag = Paths.get(exportFolderAddress + exportName + "_Tag.CSV");
        srcFiles.add(pathBook); srcFiles.add(pathAuthor); srcFiles.add(pathComment); srcFiles.add(pathTag);

        Zip.unZip(exportFolderAddress, exportName);

        String[] row = null;
        CSVReader csvReader = new CSVReader(new FileReader(pathBook.toString()));
        List content = csvReader.readAll();
        for (Object object : content) {
            row = (String[]) object;
            Integer bookID = Integer.parseInt(row[0]);
            Ebook book = new Ebook(null, row[1], row[2], row[3], Integer.parseInt(row[4]), Integer.parseInt(row[5]), row[6]);
            book.setAuthors(findAuthors(bookID, pathAuthor));
            book.setComments(findComments(bookID, pathComment));
            book.setTags(findTags(bookID, pathTag));
            bookList.add(book);
            copyPDF(bookDirectory, exportFolderAddress, exportName, book);
            Path pathPDF = Paths.get(exportFolderAddress+exportName+" - "+book.getTitle()+".pdf");
            srcFiles.add(pathPDF);
            //testPrint(row, book);
        }
        csvReader.close();
        deleteTempFiles(srcFiles);
        return bookList;
    }

    private List<Author> findAuthors(Integer bookID, Path pathAuthor) throws IOException {
        List<Author> authors = new ArrayList<>();
        String[] row = null;
        CSVReader csvReader = new CSVReader(new FileReader(pathAuthor.toString()));
        List content = csvReader.readAll();
        for (Object object : content) {
            row = (String[]) object;
            //testing
            //System.out.println("here");
            if (Integer.parseInt(row[0]) == bookID) {
                //testing
                //System.out.println("match");
                Author author = new Author(null, row[1], Integer.parseInt(row[2]));
                authors.add(author);
            }
        }
        csvReader.close();
        return authors;
    }

    private List<Comment> findComments(Integer bookID, Path pathComment) throws IOException, ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");

        List<Comment> comments = new ArrayList<>();
        String[] row = null;
        CSVReader csvReader = new CSVReader(new FileReader(pathComment.toString()));
        List content = csvReader.readAll();
        for (Object object : content) {
            row = (String[]) object;
            if (Integer.parseInt(row[0]) == bookID) {
                Comment comment = new Comment(null,row[2], dateFormat.parse(row[1]),Integer.parseInt(row[0]));
                comments.add(comment);
            }
        }
        csvReader.close();
        return comments;
    }

    private List<Tag> findTags(Integer bookID, Path pathTag) throws IOException {
        List<Tag> tags = new ArrayList<>();
        String[] row = null;
        CSVReader csvReader = new CSVReader(new FileReader(pathTag.toString()));
        List content = csvReader.readAll();
        for (Object object : content) {
            row = (String[]) object;
            if (Integer.parseInt(row[0]) == bookID) {
                Tag tag = new Tag(null, row[1], Color.valueOf(row[2]));
                tags.add(tag);
            }
        }
        csvReader.close();
        return tags;
    }

    private void copyPDF(String bookDirectory, String exportFolderAddress, String exportName, Ebook book) {
        File oldfile =new File(bookDirectory + exportName + " - " + book.getTitle() + ".pdf");
        File newfile =new File(bookDirectory + book.getTitle() + ".pdf");
        if (!newfile.exists()){
            FileCopy.copy(exportFolderAddress, bookDirectory, exportName + " - " + book.getTitle());
            oldfile.renameTo(newfile);
        }
    }

    private void testPrint(String[] row, Ebook book) {
        //print book data for test
        System.out.println(row[0] +" - "+ row[1] +" - "+ row[2] +" - "+ row[3] + " - " + row[4] +" - "+ row[5]+" - "+ row[6]);
        System.out.println(book.getAuthors().get(0).getName()+" - "+book.getAuthors().get(1).getName());
        System.out.println(book.getComments().get(0).getDate());
        System.out.println(book.getTags().get(0).getColor());
    }

    private void deleteTempFiles(List<Path> srcFiles) {
        for (Path path:srcFiles) {
            File file = new File(path.toString());
            file.delete();
        }
    }
}