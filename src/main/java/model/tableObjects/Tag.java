
package model.tableObjects;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**Tag class
 *
 * @author  Houra Keshavarz
 * @version 1.0
 * @since   2019-08-10

 */
public class Tag {
    private Integer id;
    private String label;
    private Color color;
    private List<Ebook> books;


    public Tag(Integer id, String label, Color color) {
        this.id = id;
        this.label = label;
        this.color = color;
        books =new ArrayList<>();
    }

    public Tag(Integer id, String label) {
        this(id, label, Color.RED);
    }

    public Integer getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public List<Ebook> getBooks() {
        return books;
    }
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "label='" + label + '\'' +
                ", color=" + color +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id.equals(tag.id) &&
                label.equals(tag.label) &&
                color.equals(tag.color) &&
                Objects.equals(books, tag.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, label, color, books);
    }
}