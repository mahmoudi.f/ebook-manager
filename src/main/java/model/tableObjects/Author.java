package model.tableObjects;

import java.util.Objects;

public class Author {
    private Integer id;
    private String name;
    private Integer authorOrder;

    public Author(String name, Integer authorOrder) {
        this.id = id;
        this.name = name;
        this.authorOrder = authorOrder;
    }

    public Author(Integer id, String name){
        this.id = id;
        this.name = name;
    }
    public Author(Integer id, String name, Integer authorOrder){
        this.id = id;
        this.name = name;
        this.authorOrder = authorOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return name.equals(author.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, authorOrder);
    }

    @Override
    public String toString() {
        return name;
    }

    public Author(String name){
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAuthorOrder() {
        return authorOrder;
    }

    public void setAuthorOrder(Integer authorOrder) {
        this.authorOrder = authorOrder;
    }
}
