package model.tableObjects;

import java.util.Objects;
import java.util.Date;

public class Comment {

    private Integer id;
    private String content;
    private Date date;
    private Integer book_id;

    public Comment(Integer id, String content, Date date, Integer book_id){

        this.content = content;
        this.id = id;
        this.date = date;
        this.book_id = book_id;
    }

    @Override
    public String toString() {
        return "Comment{" +
                ", id=" + id +
                "content='" + content + '\'' +
                ", date=" + date +
                ", book_id=" + book_id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id==(comment.id) &&
                content.equals(comment.content) &&
                date.equals(comment.date) &&
                book_id==(comment.book_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content, date, book_id);
    }

    public Integer getBookId() {
        return book_id;
    }

    public void setBook_id(Integer book_id) {
        this.book_id = book_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
