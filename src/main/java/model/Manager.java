package model;

import model.dao.AuthorDao;
import model.dao.CommentDao;
import model.dao.EbookDao;
import model.dao.TagDao;
import model.tableObjects.Author;
import model.tableObjects.Comment;
import model.tableObjects.Ebook;
import model.tableObjects.Tag;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public class Manager {
    private final String url = "jdbc:mysql://localhost:3306/bookdatabase";
    private final String userName = "root";
    private final String password = "1234";

    private Connection connection;
    private EbookDao ebookDao;
    private AuthorDao authorDao;
    private CommentDao commentDao;
    private TagDao tagDao;
    private Export exportBooks;
    private Import importBooks;
    private String bookDirectory;


    public Manager(){
        try {
            this.connection = DriverManager.getConnection(url, userName, password);
            ebookDao = new EbookDao(connection);
            authorDao = new AuthorDao(connection);
            commentDao = new CommentDao(connection);
            tagDao = new TagDao(connection);
            exportBooks = new Export();
            importBooks = new Import();
            createBookDirectory();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {

        return connection;
    }

    // ************ import & export ************
    public void importBooks(String exportFolderAddress, String exportName) {
        try {
            List<Ebook> bookList = importBooks.importBooks(bookDirectory, exportFolderAddress, exportName);
            addForImport(bookList);
            System.out.println("Import Successful");
        }catch (IOException ex){
            ex.printStackTrace();
        }catch (ParseException ex){
            ex.printStackTrace();
        }
    }

    public void exportBooks(List<Ebook> listForExport,String exportFolderAddress, String exportName) {
//        List<Ebook> books = fetchEbooks();
        exportBooks.exportBooks(listForExport,bookDirectory,exportFolderAddress,exportName);
        System.out.println("Export Successful");
    }

    public void addForImport(Ebook ebook){
        int bookId = storeBookInfo(ebook);
        List<Author> authors = ebook.getAuthors();
        for(Author author: authors){
            int authorId = addAuthor(author);
            addToBookAuthor(bookId, authorId, author.getAuthorOrder());
        }
    }

    public void addForImport(List<Ebook> ebooks){
        for(Ebook ebook:ebooks){
            addForImport(ebook);
        }
    }

    // ************ store book ************
    private void createBookDirectory() {
        if (Files.notExists(Paths.get(System.getProperty("user.home") + "\\Book Directory"))) {
            new File(System.getProperty("user.home") + "\\Book Directory").mkdirs();
            System.out.print("Book Directory created: ");
        }
//        }else {
//            System.out.print("Book Directory is already exists: ");
//        }
        bookDirectory = System.getProperty("user.home").toString() + "\\Book Directory\\";
        System.out.print(bookDirectory+"\n");
    }

    public void copyPdf(String sourceAddress, String fileName, String titleName){
        FileCopy.copy(sourceAddress, bookDirectory, fileName);
        File oldFile = new File(bookDirectory + fileName + ".pdf");
        File newFile = new File(bookDirectory + titleName + ".pdf");
        oldFile.renameTo(newFile);
    }

    public void openPDF(String bookName) {
        File file = new File(bookDirectory+"\\"+bookName+".pdf");
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int storeBookInfo(Ebook ebook){
        return ebookDao.addBook(ebook.getTitle(), ebook.getGenre(), ebook.getPublisher(), ebook.getEdition(), ebook.getYear(), ebook.getSummary());
    }

    public void storeBook(Ebook ebook){

        int bookId = storeBookInfo(ebook);

        for(Author author: ebook.getAuthors()){
            addToBookAuthor(bookId, author.getId(), author.getAuthorOrder());
        }

        for(Comment comment:ebook.getComments()){
            addComment(bookId, comment);
        }

        for(Tag tag:ebook.getTags()){
            addToBookTag(bookId, tag);
        }
    }

    public int deleteBookList(int bookId) {
        return ebookDao.deleteBook(bookId);
    }

    public void deleteBookList(Ebook ebook) {
//        for(Author author: ebook.getAuthors()){
//            int count = authorDao.bookAuthorCount(author.getId());
//            if(count != 1){
//                deleteBookAuthor(ebook.getId(), author.getId());
//                deleteBookList(ebook.getId());
//            }else{
//                deleteAuthor(author.getId());
//                deleteBookAuthor(ebook.getId(), author.getId());
                deleteBookList(ebook.getId());
//            }
//        }
        File file = new File(bookDirectory + "\\" + ebook.getTitle() + ".pdf");
        file.delete();

//        deleteBookList(ebook.getId());
    }

    public void deleteBookList(List<Ebook> ebooks) {
        for(Ebook ebook:ebooks){
            deleteBookList(ebook);
        }
    }

    public void editBook(Ebook ebook){
        ebookDao.editBook(ebook.getId(), ebook.getTitle(), ebook.getGenre(), ebook.getPublisher(), ebook.getEdition(), ebook.getYear(), ebook.getSummary());
    }

    // ************ filter ************
    public List<Ebook> filterByTagList(List<String> tagList){
        return ebookDao.filterByTagList(tagList);
    }

    public List<Ebook> filterByAuthorList(List<String> authorList) {
        return ebookDao.filterByAuthorList(authorList);
    }

    public List<Ebook> filterByAuthorTagList(List<String> authorList, List<String> tagList) {
        return ebookDao.filterByAuthorTagList(authorList, tagList);
    }

    // ************ author ************
    public List<Ebook> fetchEbooks(){
        List<Ebook> books = ebookDao.fetchAllBooks();
        for(Ebook ebook : books){
            ebook.setAuthors(authorDao.findAuthorsById(ebook.getId()));
            ebook.setComments(commentDao.findCommentsByBookId(ebook.getId()));
            ebook.setTags(tagDao.findTagsbyBook(ebook.getId()));
        }
        return books;
    }

    public int addAuthor(Author author){

        return authorDao.addAuthor(author);
    }

    public void editAuthor(Author author){

        authorDao.editAuthor(author);
    }

    public void deleteAuthor(int authorId){

        authorDao.deleteAuthor(authorId);
    }

    public void addAuthorList(List<Author> authors){
        for(Author author : authors){
            authorDao.addAuthor(author);
        }
    }

    public void addToBookAuthor(int bookId, int authorId, int authorOrder){
        ebookDao.addBookAuthor(bookId, authorId, authorOrder);
    }

    public void deleteBookAuthor(int bookId, int authorId){

        ebookDao.deleteBookAuthor(bookId, authorId);
    }

    public List<Author> fetchAllAuthors(){

        return authorDao.fetchAllAuthors();
    }

    public List<String> fetchAllAuthorNames(){

        return authorDao.fetchAllAuthorNames();
    }

    // ************ comment ************
    public void addComment(Integer bookId, Comment comment){

        commentDao.addComment(bookId, comment);
    }

    public void deleteComment(Integer bookId){

        commentDao.deleteComment(bookId);
    }

    public void editComment(Comment comment){

        commentDao.editComment(comment);
    }

    // ************ tag ************
    public void addToBookTag(Integer bookId, Tag tag){

        tagDao.addToBookTag(bookId, tag);
    }

    public void addTagList(List<Tag> tags) {
        for(Tag tag : tags){
            tagDao.addTag(tag);
        }
    }

    public void deleteTag(Tag tag){

        tagDao.deleteTag(tag);
    }

    public void editTag(Integer tagId, Tag tag){

        tagDao.editTag(tagId, tag);
    }

    public List<Tag> fetchAllTags(){

        return tagDao.fetchAllTags();
    }

    public List<String> fetchAllTagsName(){

        return tagDao.fetchAllTagsName();
    }



}
