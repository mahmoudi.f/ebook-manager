package model;


/**
 * 2019/08/12 monday
 * 13:00 PM to 13:10 PM
 * @author: Mohammad Ibrahimkhah
 *
 *
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    public static void copy(String sourceAddress, String destinationAddress,String fileName){
        File file = new File(fileName + ".pdf");
        File source = new File(sourceAddress + file);
        File destination = new File( destinationAddress + file);

        try (FileInputStream fis = new FileInputStream(source);
             FileOutputStream fos = new FileOutputStream(destination) )
        {
            byte[] buffer = new byte[4096];
            int read;
            while ((read = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, read);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
