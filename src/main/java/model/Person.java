package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public class Person {

    private StringProperty name = new SimpleStringProperty();
    private ObjectProperty<LocalDate> birthday = new SimpleObjectProperty<>();

    public Person() {
    }

    public Person(String name, LocalDate birthday) {
        setNameValue(name);
        setBirthdayValue(birthday);
    }

    public StringProperty getNameProperty() {
        return name;
    }

    public String getNameValue() {
        return name.getValue();
    }

    public void setNameValue(String value) {
        name.setValue(value);
    }

    public ObjectProperty<LocalDate> getBirthdayProperty() {
        return birthday;
    }

    public LocalDate getBirthdayValue() {
        return birthday.getValue();
    }

    public void setBirthdayValue(LocalDate value) {
        birthday.setValue(value);
    }

    @Override
    public String toString() {
        return getNameValue()+" ("+getBirthdayValue()+")";
    }

}
