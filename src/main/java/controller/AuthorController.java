package controller;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.tableObjects.Author;
import model.Manager;

import java.util.ArrayList;
import java.util.List;

public class AuthorController {
    Manager manager = new Manager();
    List<HBox> hBoxes = new ArrayList<>();
    List<Author> authors = new ArrayList<>();

    @FXML
    private VBox vbox;

    @FXML
    private Button OKButton;

    // triggers when OK is clicked.
    @FXML
    void addNewAuthor(MouseEvent event) {
        manager.addAuthorList(getAuthors(hBoxes));
        Stage stage = (Stage)OKButton.getScene().getWindow();
        stage.close();
    }

    public List<Author> getAuthors(List<HBox> hBoxes){
        for(HBox hBox: hBoxes){
            String name = ((TextField)hBox.getChildren().get(0)).getText();
            authors.add(new Author(null, name));
        }
        return authors;
    }

    @FXML
    void newAuthor(MouseEvent event) {
            handleNewAuthor();
    }

    public VBox handleNewAuthor(){

        HBox hBox = new HBox(10);
        hBox.setPrefWidth(360);
        hBox.setPrefHeight(35);

        TextField name = new TextField();
        name.setPromptText("name");
        name.setPrefWidth(245);
        name.setPrefHeight(25);
        name.setPadding(new Insets(5, 0, 5, 5));
        Button del = new Button("Delete");

        hBox.getChildren().addAll(name, del);
        hBoxes.add(hBox);

        vbox.getChildren().addAll(hBox);

        return vbox;
    }

}
