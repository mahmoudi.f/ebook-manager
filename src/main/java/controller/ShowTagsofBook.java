package controller;

import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.GridPane;
import model.dao.TagDao;
import model.Manager;
import model.tableObjects.Tag;

import java.util.ArrayList;
import java.util.List;

public class ShowTagsofBook {
    Label label;
    List<Tag> tags=new ArrayList<>();
    Integer id;
    TagDao tagDao =new TagDao(new Manager().getConnection());
    public ShowTagsofBook(GridPane gridPane, Integer id, Integer counter, List<Tag> tagss) {
        gridPane.getChildren().clear();
        tags=tagss;
        this.id =id;
        for(Tag tag : tagDao.findTagsbyBook(id)){
            if(gridPane.getChildren().size()<tagDao.findTagsbyBook(id).size()){
                label = new Label("#"+tag.getLabel()+"  ");
                label.setTextFill(tag.getColor());
                gridPane.add(label, counter % 7 + 1, (counter / 7) + 1);
                handleRightClick(label, tag);
                counter++;
                tags.add(tag);
            }
        }
        gridPane.setLayoutX(14);
        gridPane.setLayoutY(55);
    }
    public void handleRightClick(Label label,Tag tag) {
        ContextMenu cm = new ContextMenu();
        MenuItem delete = new MenuItem("deleteBookList");
        cm.getItems().add(delete);
        label.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent event) {
                cm.show(label, event.getScreenX(), event.getScreenY());
            }
        });
        handleDeleteTag(delete, label,tag);
    }
    private void handleDeleteTag(MenuItem mi, Label l,Tag tag) {
        mi.setOnAction(event -> {
                    l.setText("");
                    tags.remove(tag);
//                    BookTagDao bookTagDao=new BookTagDao(new Manager().getConnection());
//                    bookTagDao.deleteTagBook(tag, id);
                }
        );
    }
}
