package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.*;
import model.tableObjects.Author;
import model.tableObjects.Comment;
import model.tableObjects.Ebook;
import model.tableObjects.Tag;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookController {

    Stage bookController = new Stage();
    Manager manager = new Manager();
    Ebook ebook = new Ebook();
    List<Author> authorsList = new ArrayList<>();
    List<Comment> comments;
    List<Tag> tagsList =new ArrayList<>();
    static Integer help =1;
    static Integer counter=1;
    GridPane tagGridPane =new GridPane();
    File savedFile;


    // ************* book info *************
    @FXML
    private TextField title;
    @FXML
    private TextField edition;
    @FXML
    private TextField year;
    @FXML
    private TextField genre;
    @FXML
    private TextField publisher;
    @FXML
    private TextArea summary;
    @FXML
    private Label address;
    @FXML
    private Button saveBook;
    @FXML
    // ************* author tab *************
    private ComboBox<Author> authorBox;
    @FXML
    private TextField authorOrderBox;
    // ************* tag tab *************
    @FXML
    private ComboBox tagBox;
    // ************* comment tab *************
    @FXML
    private Button AddCommentButton;
    @FXML
    private TextArea Content;
    @FXML
    private VBox vboxComment;
    SimpleDateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy HH:mm");
    private List<Comment> commentsList = new ArrayList<>();

    // ****************************** Author *************************************
    @FXML
    void showAuthors(MouseEvent event) {
        authorBox.getItems().clear();
        authorBox.getItems().addAll(manager.fetchAllAuthors());
    }

    @FXML
    void addNewAuthors(MouseEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/forms/addNewAuthorForm.fxml"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        bookController.setScene(new Scene(root));
        bookController.show();
    }

    @FXML
    void addAuthor(MouseEvent event) {
        Integer id = authorBox.getSelectionModel().getSelectedIndex();
        Author author = manager.fetchAllAuthors().get(id);
        author.setAuthorOrder(Integer.parseInt(authorOrderBox.getText()));
        authorsList.add(author);
    }

    // ******************************** Tag ***********************************
    @FXML
    void addTag(MouseEvent event) {
        Integer id = tagBox.getSelectionModel().getSelectedIndex();
        Tag tag = manager.fetchAllTags().get(id);
        tagsList.add(tag);
    }

    @FXML
    void showTags(MouseEvent event) {
        tagBox.getItems().clear();
        tagBox.getItems().addAll(manager.fetchAllTagsName());
    }

    // ******************************** Comment ***********************************
    @FXML
    void addComment(MouseEvent event){
        System.out.println("in addComment");

        AddCommentButton.setOnMouseClicked(e->{
            try {
                System.out.println("in OKButton");
                Comment c = new Comment(null, Content.getText(), new Date(), null);
                commentsList.add(c);

                TextField tf = new TextField(Content.getText());
                Button edit = new Button("Edit");
                Button delete = new Button("Delete");
                Label dateLbl = new Label(dateFormat.format(c.getDate()));

                Content.clear();
                HBox row = new HBox();
                row.getChildren().addAll(dateLbl, tf, edit, delete);
                vboxComment.getChildren().add(row);


                edit.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        try {

                            Stage form = new Stage();
                            FXMLLoader fx = new FXMLLoader(getClass().getResource("/forms/editCommentForm.fxml"));
                            Parent parent = fx.load();

                            //Get view of next scene
                            EditCommentController editCommentController = fx.getController();
                            //show current comment in next scene's textArea
                            editCommentController.setComment(c.getContent());

                            form.setScene(new Scene(parent));
                            form.showAndWait();

                            //update comment's date and content
                            c.setContent(editCommentController.getComment());
                            c.setDate(new Date());
                            tf.setText(c.getContent());


                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    }

                });

                delete.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        commentsList.remove(c);
                        vboxComment.getChildren().remove(row);

                    }
                });

            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
    }

    // ******************************** Book ***********************************
    @FXML
    void browse(MouseEvent event) {
        try {
            Stage form = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose PDF");
            savedFile = fileChooser.showOpenDialog(form);
            address.setText(savedFile.getPath());
            if (savedFile != null) {
                System.out.println(savedFile.getParent()+ " - " + savedFile.getName().replaceAll(".pdf",""));
            }
            else {
                System.out.println("copy cancelled.");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @FXML
    void saveNewBook(MouseEvent event) {
        saveBookInfo();
        ebook.setAuthors(authorsList);
        ebook.setComments(commentsList);
        ebook.setTags(tagsList);
        manager.storeBook(this.ebook);
        System.out.println(title.getText());
        manager.copyPdf(savedFile.getParent()+"\\", savedFile.getName().replaceAll(".pdf", ""), title.getText());

        Stage stage = (Stage)saveBook.getScene().getWindow();
        stage.close();
    }

    private void saveBookInfo(){
        ebook.setTitle(title.getText());
        ebook.setEdition(Integer.parseInt(edition.getText()));
        ebook.setYear(Integer.parseInt(year.getText()));
        ebook.setGenre(genre.getText());
        ebook.setPublisher(publisher.getText());
        ebook.setSummary(summary.getText());
    }


}
