package controller;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.Manager;
import model.tableObjects.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagController {

    Manager manager = new Manager();
    List<HBox> hBoxes = new ArrayList<>();
    List<Tag> tags = new ArrayList<>();

    @FXML
    private VBox vbox;

    @FXML
    private Button OKButton;


    // triggers when OK Button is clicked.
    @FXML
    void addTagList(MouseEvent event) {
        manager.addTagList(getTags(hBoxes));
        Stage stage = (Stage)OKButton.getScene().getWindow();
        stage.close();
    }

    public List<Tag> getTags(List<HBox> hBoxes){
        for(HBox hBox: hBoxes){
            String name = ((TextField)hBox.getChildren().get(0)).getText();
            tags.add(new Tag(null, name));
        }
        return tags;
    }

    //triggers when NEW button is clicked.
    @FXML
    void newTag(MouseEvent event) {
        handleNewTag();
    }

    public VBox handleNewTag(){

        HBox hBox = new HBox(10);
        hBox.setPrefWidth(360);
        hBox.setPrefHeight(35);

        TextField name = new TextField();
        name.setPromptText("name");
        name.setPrefWidth(245);
        name.setPrefHeight(25);
        name.setPadding(new Insets(5, 0, 5, 5));

        ColorPicker tagColor = new ColorPicker(Color.RED);
        tagColor.setLayoutX(155);
        tagColor.setLayoutY(14);

        Button del = new Button("Delete");

        hBox.getChildren().addAll(name, tagColor, del);
        hBoxes.add(hBox);

        vbox.getChildren().addAll(hBox);

        return vbox;
    }

}
