package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class EditCommentController {

    @FXML
    private Button OKButton;
    @FXML
    private Button CancelButton;
    @FXML
    private TextArea editArea;

    private EventHandler<? super MouseEvent> closeEditCommentHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent mouseEvent) {
            final Node source = (Node) mouseEvent.getSource();
            final Stage stage = (Stage) source.getScene().getWindow();
            stage.close();
        }
    };


    public EditCommentController() {
    }

    public void setComment(String s) {
        editArea.setText(s);
    }

    public String getComment(){

        return editArea.getText();
    }

    public void editComment(){

        OKButton.setOnMouseClicked(closeEditCommentHandler);
    }


    public void cancelEdit(){

        CancelButton.setOnMouseClicked(closeEditCommentHandler);


    }

}


